# Tinkoff Homework - Mikhail Shimkiv
## What is this repo about?
This repository contains autotests for /providers?groups API method

## Relations/dependencies to other repos
https://github.com/scalaj/scalaj-http
https://github.com/scalatest/scalatest
https://github.com/spray

## How to build
Nothing to build yet

## How to run
Run ApiProvidersTest with Scala

## Who is author/maintainer
Mikhail Shimkiv

## Links to documentation
TBD