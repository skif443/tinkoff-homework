name := "ScalaTest"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.1"

libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.3.0"

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.3"