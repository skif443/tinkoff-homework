import scalaj.http.{Http, HttpResponse}

package rest_api {

  /** Create instance of RestManager to use REST-based methods
    * @param base_url Rest API base url*/
  class RestManager(base_url: String) {

    /**
      *
      * @param url Corresponding method's url
      * @return HTTP Response from REST service
      */
    def send_get_request(url: String): HttpResponse[String] = {
      val full_url: String = base_url + url
      Http(full_url).asString
    }
  }
}