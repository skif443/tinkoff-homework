import org.scalatest.{FreeSpec, Matchers}
import spray.json._
import scalaj.http.HttpResponse
import spray.json.JsonParser.ParsingException

import rest_api.RestManager


/**
  * Autotests based on ScalaTest framework with using FreeSpec
  */

class ApiProvidersTest extends FreeSpec with Matchers{

  val BASE_URL: String = "https://www.tinkoff.ru/api/v1/"
  val ALLOWED_GROUPS = Array("Переводы", "Интернет", "Благотворительность")
  val rest_mgr = new RestManager(BASE_URL)

  /**
    * Check if providerFields "id" and "name" from "/providers?groups=<>" request corresponds
    * to special rules: for each id which is equal to "lastName", name field should contain "Фамилия" substring
    * @param elements Vector of JsValues in which fields should be found
    * @param original_string String which should be found in "id" field
    * @param substring Substring which should be found in "name" field
    */
  def check_substring (elements: Vector[JsValue], original_string: String, substring: String): Unit = {
    elements.foreach((item: JsValue) => {
      val provider_fields = item.asJsObject().fields(
        "providerFields")
      val provider_fields_elements = provider_fields.asInstanceOf[JsArray].elements
      provider_fields_elements.foreach((provider_field_item: JsValue) => {
        val provider_field_id = provider_field_item.asJsObject.fields("id")
        if (provider_field_id.asInstanceOf[JsString].value == original_string) {
          val provider_name_field = provider_field_item.asJsObject.fields("name")
          provider_name_field.asInstanceOf[JsString].value.contains(substring) should be (true)
        }
      })
  })
  }


  // Test will be applied to every group that allowed

  ALLOWED_GROUPS.foreach((group: String) => {

    val response:HttpResponse[String] = rest_mgr.send_get_request(
      s"providers?groups=$group"
    )

    // Test #1 - Check if HTTP result code is 2XX
     s"Result code for group $group should be 2XX (OK)" in {
       response.is2xx should be (true)
      }

    // Test #2 - Check if response body is JSON
    s"Response body is JSON for $group" in {
      try {
        val response_parsed_to_json = response.body.parseJson.asJsObject
      } catch {
        case _: ParsingException => fail("Response body is not in JSON format")
      }
    }

    // Test #3 - Check if resultCode value from JSON response is OK
     s"resultCode value from JSON response is equal to OK for $group" in {
       val result_code_from_json = response.body.parseJson.asJsObject.fields("resultCode")
       result_code_from_json.asInstanceOf[JsString].value should be ("OK")
     }

    // Test #4 - Check if every groupId field is equal to group name
    s"Every groupId field is equal to group name for $group" in {
      val payload_field = response.body.parseJson.asJsObject.fields("payload")
      val elements = payload_field.asInstanceOf[JsArray].elements
      elements.foreach((item: JsValue) => {
        val group_id_field = item.asJsObject.fields("groupId")
        group_id_field.asInstanceOf[JsString].value should be (group)
      })
    }

    // Test #5 - Check if substring contains "Фамилия" for every id that
    // is equal to lastName
    s"substring contains Фамилия for every id = lastName for $group" in {
      val payload_field = response.body.parseJson.asJsObject.fields("payload")
      val first_level_elements = payload_field.asInstanceOf[JsArray].elements

      // To check this code fill original_string = "dstCardId", substring="вывода"
      check_substring(first_level_elements, "lastName", "Фамилия")

    }}
  )
}
